PortOpener

Programa para abrir y routear puertos mediante UPNP y NAT a una PC temporalmente y de forma rápida y sencilla.

Requisitos:
.NET Core - versión 2.1.4 o superiores recomendadas.
https://www.microsoft.com/net/learn/get-started/windows

Para ejecutar sin compilar:
En la carpeta del código:
dotnet run (números de los puertos separados por espacios)
Ejemplo:
dotnet run 80 442 8080 6112 25015

Comandos especiales:
.w ó war3: abre los puertos necesariós para el Warcraft III de forma predeterminada.
Ejemplos:
dotnet run w
dotnet run war3

Para compilar el programa:
dotnet restore
dotnet publish -c Release -r (string de la plataforma a compilar)

El programa compilado con sus dependencias estara disponible en la carpeta .\bin\Release\netcoreapp1.1<runtime_identifier>

Plataformas habilitadas por el proyecto:
win7-x86;win7-x64;win81-x86;win81-x64;win10-x86;win10-x64;osx.10.11-x64;osx.10.12-x64;ubuntu.16.04-x86;ubuntu.16.04-x64
