﻿/*
This file is part of PortOpener.

PortOpener Copyright (C) 2018  Pablo Pedrotti pablonpedrotti@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;

namespace PortOpener
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PortOpener Copyright (C) 2018  Pablo Pedrotti");
            Console.WriteLine("This program comes with ABSOLUTELY NO WARRANTY");
            Console.WriteLine("This is free software, and you are welcome to redistribute it under certain conditions.");
            Console.WriteLine("Los puertos introducidos se mantendran abiertos mientras este programa este en ejecución.");
            var cts = new CancellationTokenSource(10000);
            var discoverer = new NatDiscoverer();
            //var device = discoverer.DiscoverDeviceAsync(PortMapper.Upnp, cts);
            var t = Task.Run(async () =>
            {
                var device = await discoverer.DiscoverDeviceAsync(PortMapper.Upnp, cts);
                var ip = await device.GetExternalIPAsync();
                Console.WriteLine("La dirección IP externa es: {0} ", ip);
                foreach(string port in args){
                    int iport;
                    if (Int32.TryParse(port, out iport) == true) {
                        await device.CreatePortMapAsync(new Mapping(Protocol.Tcp, iport, iport, 216000, "TCP " + port));
                        await device.CreatePortMapAsync(new Mapping(Protocol.Udp, iport, iport, 216000, "UDP " + port));
                        Console.WriteLine("Se abrio el puerto " + port + " exitosamente.");
                    }
                    if (port == "w" || port == "war3") {
                        await device.CreatePortMapAsync(new Mapping(Protocol.Tcp, 6112, 6112, 216000, "TCP " + 6112));
                        Console.WriteLine("Se abrio el puerto predeterminado para el Warcraft III (6112) exitosamente.");
                    }
                }
            });
            Console.ReadLine();
            NatDiscoverer.ReleaseAll();
            Console.WriteLine("Se cerraron todos los puertos abiertos.");
        }
    }
}
